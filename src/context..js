import React, {useState, useContext, useEffect} from 'react';
import { useCallback } from 'react';
const URL = process.env.REACT_APP_API_URL;
const apiKey = process.env.REACT_APP_API_KEY;
const AppContext = React.createContext();

const AppProvider = ({children}) => {
    const [searchTerm, setSearchTerm] = useState("Amazing Book");
    const [books, setBooks] = useState([]);
    const [loading, setLoading] = useState(true);
    const [resultTitle, setResultTitle] = useState("");

    const fetchBooks = useCallback(async() => {
        setLoading(true);
        try{
            const response = await fetch(`${URL}/books?keyword=${searchTerm}&limit=20`, {
                method: 'get',
                headers: new Headers({
                    'x-api-key': apiKey
                })
            });
            const dataEncoded = await response.json();
            const data = atob(dataEncoded.data);
            const dataParsed = JSON.parse(data);
            const list = dataParsed.books;

            if(list){
                const newBooks = list.map((bookSingle) => {
                    const {title,author,rating,thumbnail} = bookSingle;

                    return {
                        author: author,
                        cover_img: thumbnail,
                        rating: rating,
                        title: title
                    }
                });

                setBooks(newBooks);

                if(newBooks.length > 1){
                    setResultTitle("Your Search Result");
                } else {
                    setResultTitle("No Search Result Found!")
                }
            } else {
                setBooks([]);
                setResultTitle("No Search Result Found!");
            }
            setLoading(false);
        } catch(error){
            console.log(error);
            setLoading(false);
        }
    }, [searchTerm]);

    useEffect(() => {
        fetchBooks();
    }, [searchTerm, fetchBooks]);

    return (
        <AppContext.Provider value = {{
            loading, books, setSearchTerm, resultTitle, setResultTitle,
        }}>
            {children}
        </AppContext.Provider>
    )
}

export const useGlobalContext = () => {
    return useContext(AppContext);
}

export {AppContext, AppProvider};