import React from 'react';
import { Link } from 'react-router-dom';
import "./BookList.css";
import ReactStars from "react-rating-stars-component";

const Book = (book) => {
  const saveBook = async () => {
    await fetch(
      `http://it.iki.id:1133/v1/books/favorite`,
       {
        method: 'post',
        headers: new Headers({
          'Content-Type': 'application/json',
          'x-api-key': 'iniapikey',  
        }),
        body: JSON.stringify({
          title: book.title,
          author: book.author,
          rating: book.rating,
          thumbnail: book.cover_img
        }),
        
       }
    );
    console.log(JSON.stringify({
      title: book.title,
      author: book.author,
      rating: book.rating,
      thumbnail: book.cover_img
    }))

    alert(`Success Save ${book.title}`);
  };
  return (
    <div className='book-item flex flex-column flex-sb'>
      <div className='book-item-img'>
        <img src = {book.cover_img} alt = "cover" />
      </div>
      <div className='book-item-info text-center'>
        <Link to = {`/book/${book.id}`} {...book}>
          <div className='book-item-info-item title fw-7 fs-18'>
            <span>{book.title}</span>
          </div>
        </Link>

        <div className='book-item-info-item author fs-15'>
          <span className='text-capitalize fw-7'>Author: </span>
          <span>{book.author}</span>
        </div>

        <div className='book-item-info-item rating fs-15'>
          <span className='text-capitalize fw-7'>Rating: </span>
        </div>

        <div className='book-item-info-item rating_icon fs-15'>
         <ReactStars count={5} value={book.rating} size={15} isHalf={false} edit = {false}/>
        </div>

        <div className='book-item-info-item save fs-15'>
          <button className='book-item-button' onClick={saveBook}>Save</button>
        </div>

        


      </div>
    </div>
  )
}

export default Book